import unittest

from pathlib import Path
from utils import WordsCounter


class WordsCounterTests(unittest.TestCase):

    BASE_DIR = Path(__file__).parent
    VALID_RESULT = {
        'file': BASE_DIR / 'test_text.txt',
        'result': [
            ('the', 3150), ('a', 2025), ('of', 1800), ('is', 1125), ('must', 1125), ('occurrences', 1125),
            ('be', 900), ('in', 900), ('number', 900), ('script', 900), ('with', 900), ('and', 675),
            ('python', 675), ('task', 675), ('that', 675), ('to', 675), ('we', 675), ('word', 675), ('words', 675),
            ('as', 450), ('candidate', 450), ('complete', 450), ('from', 450), ('it', 450), ('line', 450),
            ('order', 450), ('out', 450), ('output', 450), ('program', 450), ('result', 450), ('sorted', 450),
            ('text', 450), ('their', 450), ('will', 450), ('3', 225), ('all', 225), ('alphabetic', 225),
            ('also', 225), ('an', 225), ('apart', 225), ('assignment', 225), ('at', 225), ('believe', 225),
            ('better', 225), ('by', 225), ('can', 225), ('candidates', 225), ('carefully', 225), ('checking', 225),
            ('command', 225), ('descending', 225), ('desirable', 225), ('developed', 225), ('don', 225),
            ('each', 225), ('equal', 225), ('file', 225), ('format', 225), ('frequency', 225), ('help', 225),
            ('hour', 225), ('input', 225), ('intended', 225), ('interface', 225), ('its', 225), ('level', 225),
            ('more', 225), ('new', 225), ('no', 225), ('on', 225), ('operability', 225), ('pre-interview', 225),
            ('print', 225), ('printed', 225), ('professional', 225), ('programmer', 225), ('programming', 225),
            ('read', 225), ('reading', 225), ('screen', 225), ('simple', 225), ('skills', 225), ('solution', 225),
            ('stage', 225), ('statistics', 225), ('t', 225), ('take', 225), ('test', 225), ('than', 225),
            ('think', 225), ('this', 225), ('understand', 225), ('unique', 225), ('us', 225), ('uses', 225),
            ('very', 225)]
    }

    @classmethod
    def setUpClass(cls):
        cls.counter = WordsCounter()

    def test_calc_with_collections(self):
        for func in (self.counter.calc_with_collections, self.counter.calc_with_dict):
            # для тестов с незначительными отличиями (напр. некоторыми параметрами)
            with self.subTest(counter=func):
                self.assertEqual(self.VALID_RESULT['result'],
                                 func(self.VALID_RESULT['file']),
                                 msg='Некорректный подсчёт')
