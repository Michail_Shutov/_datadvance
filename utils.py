import re
import os

from collections import Counter, defaultdict


class WordsCounter:
    """
    класс реализующий методы для подсчёта количества слов
    calc_with_collections - когда важна скорость
    calc_with_dict - когда важна память (работает через итератор)

    Скорость:
     - на маленьких файлах (в пару строк) через словарь быстрее
     - на средних файлах быстрее через collections.Counter
     - на ооочень больших файлах лучше через словарь, т.к. экономит память
    """

    words_re = re.compile('(?:\w|[-_])+')  # a-zA-Z не отрабатывают не-английские буквы

    def __load_text(self, file_path: str, as_iterator=True):
        """
        загружает текстовый файл
        с помощью данного метода можно перезагрузить переданный в конструкторе файл
        """
        with open(file_path) as file:  # в 3.0 file - не зарезервированное имя
            text = file.read().lower()  # чтобы все слова были в одном регистре
            func = re.finditer if as_iterator else re.findall
            return func(self.words_re, text)

    def calc_with_collections(self, file_path):
        """
        Считает через collections.Counter
        """
        words = self.__load_text(file_path, as_iterator=False)
        c = Counter(words)
        result = c.most_common()
        result.sort(key=lambda x: (-1*x[1], x[0]))  # методом быстрее sorted
        return result

    def calc_with_dict(self, file_path):
        """
        Считает через словарь
        Медленнее, зато можут работать с итератор,
        т.е. не будет переполнения памяти в случае большого файла
        """
        result = defaultdict(lambda: 0)
        for word in self.__load_text(file_path):
            result[word.group()] += 1
        result = sorted(result.items(), key=lambda x: (-1*x[1], x[0]))
        return result


if __name__ == '__main__':

    default_file = './test_text.txt'
    while True:
        path_to_file = input(f'Введите путь к файлу (default: {default_file}):'
                             f'\n>>> ')
        path_to_file = path_to_file or default_file
        if not (os.path.exists(path_to_file) and os.path.isfile(path_to_file)):
            print('некорректное имя файла')
        else:
            break

    counter = WordsCounter()

    print(counter.calc_with_collections(path_to_file))

    for word, count in counter.calc_with_collections(path_to_file):
        print(f'{word}: {count}')
